import request from 'request-promise-native';
import format from 'date-fns/format';

const jar = request.jar();
const phpSessIdCookie = request.cookie('PHPSESSID=8c8pomubf0bc2hnv2a5e9feu66');
const url =
  'https://lewam.extraclub.fr/fo/prive/menu/reserver_site/includer.php';
jar.setCookie(phpSessIdCookie, url);

export const getReservations = async ({ params }) => {
  const response = await request.get(url, {
    jar,
    qs: {
      inc: 'ajax/reservation/planning3',
      ressourceSelected: params.resources || '58-69-70-71',
      date: params.date,
    },
  });

  const responseBody = JSON.parse(response).reservation;

  const reservations = Object.keys(responseBody)
    .map(id => responseBody[id])
    .map(({ creneauId, reservataire }) => {
      const slot = creneauId.split('#');

      return {
        resources: slot[0].split('-').map(Number),
        from: format(Date.parse(slot[1])),
        to: format(Date.parse(slot[2]) - 1),
        participants: reservataire.map(({ nom, infoBulle, personneId }) => ({
          name: nom,
          id: Number(personneId),
          type: infoBulle.PersonneTypeMembre,
        })),
      };
    });

  return reservations;
};
