import express from 'express';
const app = express();
import request from 'request-promise-native';
import format from 'date-fns/format';

import { getReservations } from './api';

app.get('/bookings', async (req, res) => {
  const reservations = await getReservations({ params: req.query });

  res.json(reservations);
});

app.get('/availability', async (req, res) => {
  const reservations = await getReservations({ params: req.query });
  const date = req.query.date;
  const resources = req.query.resources
    ? req.query.resources.split('-').map(Number)
    : [58, 69, 70, 71];

  const bookableSlots = [
    {
      from: format(Date.parse(`${req.query.date} 12:00:00`)),
      to: format(Date.parse(`${req.query.date} 12:45:00`) - 1),
    },
    {
      from: format(Date.parse(`${req.query.date} 12:45:00`)),
      to: format(Date.parse(`${req.query.date} 13:30:00`) - 1),
    },
  ];

  const availableSlots = []
    .concat(
      ...resources.map(resource =>
        bookableSlots.map(({ from, to }) => ({
          from,
          to,
          resource,
        })),
      ),
    )
    .filter(
      slot =>
        !reservations.find(
          reservation =>
            reservation.from < slot.to &&
            reservation.to > slot.from &&
            reservation.resources.includes(slot.resource),
        ),
    );

  res.json(availableSlots);
});

const port = process.env.PORT || 3000;

app.listen(port, err => {
  if (err) {
    console.error(err);
  }

  if (__DEV__) {
    console.log('> in development');
  }

  console.log(`> listening on port ${port}`);
});
